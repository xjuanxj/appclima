//
//  PorCiudadViewController.swift
//  Appclima
//
//  Created by Juan Sebastian Benavides Cardenas on 6/11/17.
//  Copyright © 2017 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit

class PorCiudadViewController: UIViewController {

    @IBOutlet weak var etiquetaClima: UILabel!
    @IBOutlet weak var txtCiudad: UITextField!
    
    @IBAction func Consultar(_ sender: Any)
    {
        //obtenerClima(txtCiudad.text!)
        let servic = Servicios()
        servic.getWeatherbyCity(city:self.txtCiudad.text!) { (weather) in DispatchQueue.main.async {
            self.etiquetaClima.text! = weather
            }
           
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func obtenerClima(_ ciudad:String)
    {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(ciudad)&appid=5ec75a98086710c7d5ee8c6dd9cb94d3"
        let url  = URL(string: urlString)
        let request = URLRequest(url: url!);
        let task = URLSession.shared.dataTask(with: request){
            (data, response, error) in
            
            if let _  = error //es una alerte para evitar que el programa se caiga de tipo try cathch
            {
                return
            }
            let weatherData = data as! Data
            
            // Forma correcta del try catch
            do
            {
                let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options:[])
                let weatherdictionary = weatherJson as! NSDictionary
                let weatherArray = weatherdictionary["weather"]  as! NSArray //forma de casta con signo "!"
                let weather  = weatherArray[0] as! NSDictionary
                let weatherDes = weather["description"] ?? "error"// forma de romper opcionales con doble signo de interrogacion
                
                print(weatherDes)
                DispatchQueue.main.async {
                    self.etiquetaClima.text = "\(weatherDes)" //cast a any
                }
            }catch
            {
                print("errir en el JSON")
            }
            
            
            
            
            
            //
        }
        
        task.resume() // invocacion del metodo
        
        
    }

    @IBAction func Atras(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
    }
}
