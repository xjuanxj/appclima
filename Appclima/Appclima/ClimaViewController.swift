//
//  ClimaViewController.swift
//  Appclima
//
//  Created by Juan Sebastian Benavides Cardenas on 27/10/17.
//  Copyright © 2017 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit
import CoreLocation // ubicacion

class ClimaViewController: UIViewController, CLLocationManagerDelegate
{

 let locationManager = CLLocationManager()
    var cambioClima = false
    @IBOutlet weak var Clima: UILabel!
    @IBOutlet weak var labelUbicacion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse //opcion cuando el usuario acepta el permiso
        {
            locationManager.delegate = self
            locationManager.startUpdatingLocation() // usa clases delegadas para capturar la ubicacion
            
            
        }
        

        // Do any additional setup after loading the view.
    }
    //Mark: -CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let serv = Servicios()
        
      let location = manager.location?.coordinate
      print("la ubicacion es \(location)")
      if !cambioClima
      {
        
        serv.getweatherbyLocation(lat: (location?.latitude)!, lon: (location?.longitude)! ){ (weather, ciudad) in DispatchQueue.main.async {
            self.Clima.text! = weather
            self.labelUbicacion.text! = ciudad!
            }
        }
            cambioClima = true
    }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func obtenerClima(lat:Double, lon:Double)
    {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=5ec75a98086710c7d5ee8c6dd9cb94d3"
        let url  = URL(string: urlString)
        let request = URLRequest(url: url!);
        let task = URLSession.shared.dataTask(with: request){
            (data, response, error) in
            
            if let _  = error //es una alerte para evitar que el programa se caiga de tipo try cathch
            {
               return
            }
            let weatherData = data as! Data
            
            // Forma correcta del try catch
            do
           {
            let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options:[])
            let weatherdictionary = weatherJson as! NSDictionary
            //print(weatherJson)//impresion del jason
            let ubicacion = weatherdictionary["name"]  as!String //forma de casta con signo "!"
            
            
            
            //print(ubicacion)
            DispatchQueue.main.async {
                self.labelUbicacion.text = "\(ubicacion)" //cast a any
            }
            }catch
            {
                print("errir en el JSON")
            }
            
            
            
            
            
            //
        }
        
        task.resume() // invocacion del metodo
        
        
    }



}
