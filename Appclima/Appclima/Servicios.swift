//
//  Servicios.swift
//  Appclima
//
//  Created by Juan Sebastian Benavides Cardenas on 10/11/17.
//  Copyright © 2017 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit

class Servicios
{
    func getWeather(urlString: String, completion: @escaping(String,String?) -> ())//el @escaping permite salir de un hilo esto sirve para retornar datos entre hilos
    {
    
        let url  = URL(string: urlString)
        let request = URLRequest(url: url!);
        let task = URLSession.shared.dataTask(with: request)
           {
                (data, response, error) in
            
                if let _  = error //es una alerte para evitar que el programa se caiga de tipo try cathch
                {
                    return
                }
                let weatherData = data as! Data
            
                // Forma correcta del try catch
                do
                {
                    let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options:[])
                    let weatherdictionary = weatherJson as! NSDictionary
                    let weatherArray = weatherdictionary["weather"]  as! NSArray //forma de casta con signo "!"
                    let weather  = weatherArray[0] as! NSDictionary
                    let weatherDes = weather["description"] ?? "error"// forma de romper opcionales con doble signo de interrogacion
                    
                    let weatherJson1 = try JSONSerialization.jsonObject(with: weatherData, options:[])
                    let weatherdictionary1 = weatherJson1 as! NSDictionary
                    //print(weatherJson)//impresion del jason
                    let ubicacion = weatherdictionary["name"]  as!String //forma de casta con signo "!"
                   completion(weatherDes as! String, ubicacion)
                    
                    //DispatchQueue.main.async {
                    //    self.Clima.text = "\(weatherDes)" //cast a any
                    //}
                }catch
                {
                    print("errir en el JSON")
                }
            
            
            
            
            
                //
            }
        
        task.resume() // invocacion del metodo
        
    }
   
    
    func getWeatherbyCity(city: String, completion: @escaping (String) -> ())
    {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=5ec75a98086710c7d5ee8c6dd9cb94d3"
        getWeather(urlString: urlString) { (weather,ciudad) in
            completion(weather)
        }
    }
    
    func getweatherbyLocation(lat:Double, lon:Double,completion: @escaping (String, String?) -> ())
    {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=5ec75a98086710c7d5ee8c6dd9cb94d3"
        getWeather(urlString: urlString) { (weather,ciudad) in
            completion(weather, ciudad)
        }
    }
    
}
